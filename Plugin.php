<?php

namespace Empu\Printer;

use Backend\Facades\Backend;
use Empu\Printer\Models\Settings;
use System\Classes\PluginBase;

/**
 * Print Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Printer',
            'description' => 'Cetak via website',
            'author'      => 'Wuri Nugrahadi',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(\Barryvdh\Snappy\ServiceProvider::class);

        $this->registerConsoleCommand('printer.test', Console\TestPrintCommand::class);
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return []; // Remove this line to activate

        return [
            'Empu\Print\Components\MyComponent' => 'myComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'empu.print.access_settings' => [
                'tab' => 'Print',
                'label' => 'Ubah pengaturan pencetakan',
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'print' => [
                'label'       => 'Print',
                'url'         => Backend::url('empu/print/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['empu.print.*'],
                'order'       => 500,
            ],
        ];
    }

    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'Print Settings',
                'description' => 'Manage printing settings.',
                'category'    => 'Print',
                'icon'        => 'icon-printer',
                'class'       => Settings::class,
                'order'       => 500,
                'keywords'    => 'printer',
                'permissions' => ['empu.printer.access_settings'],
            ],
        ];
    }
}
