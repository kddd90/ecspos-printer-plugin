<?php

namespace Empu\Printer\Behaviors;

use Empu\Printer\Contracts\EscTemplate;
use Mike42\Escpos\PrintConnectors\DummyPrintConnector;
use Mike42\Escpos\Printer;
use October\Rain\Extension\ExtensionTrait;

class EscPrintBuffer
{
    use ExtensionTrait;

    protected $controller;

    /** @var \Mike42\Escpos\Printer */
    protected $printer;

    protected $connector;

    public function __construct($controller)
    {
        $this->controller = $controller;
        $this->connector = new DummyPrintConnector();
        $this->printer = new Printer($this->connector);
        $this->printer->initialize();
    }

    /**
     * Get printer
     *
     * @return \Mike42\Escpos\Printer
     */
    public function getPrinter(): Printer
    {
        return $this->printer;
    }

    public function addPrinterAssets()
    {
        $this->controller->addJs('/plugins/empu/printer/assets/js/websocketPrinter.js');
    }

    public function getPrintBufferString(EscTemplate $template, array $vars = []): string
    {
        $template->draft($this->printer, $vars);

        $bufferString = $this->connector->getData();

        $this->printer->close();

        return base64_encode($bufferString);
    }
}