<?php

namespace Empu\Printer\Models;

use October\Rain\Database\Model;

class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    // A unique code
    public $settingsCode = 'empu_printer_settings';

    // Reference to field configuration
    public $settingsFields = 'fields.yaml';
}
