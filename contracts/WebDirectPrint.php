<?php

namespace Empu\Printer\Contracts;

interface WebDirectPrint
{
    /**
     * Get name of template
     *
     * @return string
     */
    public function name(): string;

    /**
     * Get map key for multiple printers support
     *
     * @return string
     */
    public function printerMapKey(): string;
}