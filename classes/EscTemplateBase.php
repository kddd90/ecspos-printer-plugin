<?php

namespace Empu\Printer\Classes;

use Empu\Printer\Helpers\DefinitionList;
use Empu\Printer\Helpers\Table;
use Mike42\Escpos\Printer;

abstract class EscTemplateBase
{
    /**
     * @var \Mike42\Escpos\Printer
     */
    protected $printer;

    /**
     * Maximum chars per line
     *
     * @var int|null
     */
    protected $width;

    public function init(Printer $printer, ?int $width = null): void
    {
        $this->printer = $printer;
        $this->width = $width;
    }

    protected function pseudoWidth()
    {
        return !is_null($this->width);
    }

    public function newLine($feed = 1)
    {
        $this->printer->text(str_repeat("\n", $feed));
    }

    public function line($text, int $mode = null, ?int $justification = null)
    {
        $text = $this->pseudoAlignment($text, $justification);

        if (!$this->pseudoWidth() && !is_null($justification)) {
            $this->printer->setJustification($justification);
        }

        $this->text($text, $mode);
        $this->newLine();

        if (!$this->pseudoWidth() && !is_null($justification)) {
            $this->printer->setJustification();
        }
    }

    protected function pseudoAlignment($text, ?int $justification): string
    {
        if (!$this->pseudoWidth() || is_null($justification)) {
            return $text;
        }

        if ($justification === Printer::JUSTIFY_CENTER) {
            $padPossition = STR_PAD_BOTH;
        }
        elseif ($justification === Printer::JUSTIFY_RIGHT) {
            $padPossition = STR_PAD_LEFT;
        }
        else {
            $padPossition = STR_PAD_RIGHT;
        }

        return str_pad($text, $this->width, ' ', $padPossition);
    }

    public function text(string $str, ?int $mode = null)
    {
        if (!is_null($mode)) {
            $this->printer->selectPrintMode($mode);
        }

        $this->printer->text($str);

        if (!is_null($mode)) {
            $this->printer->selectPrintMode();
        }
    }

    public function definitionList(array $list, int $keyWidth = 20, ?int $lineWidth = null): DefinitionList
    {
        $dl = new DefinitionList($this->printer);

        $dl->printList($list, $keyWidth, $lineWidth);

        return $dl;
    }

    public function table($rows, array $config, bool $finish = true): ?Table
    {
        $table = new Table($this->printer, $config);

        $table->printTable($rows, $finish);

        return $finish ? null : $table;
    }
}