<?php

namespace Empu\Printer\Classes;

use Empu\MedCore\Models\Settings;

/**
 * @deprecated v1.0.2
 */
abstract class PrintTemplate extends EscTemplateBase
{
    /**
     * Template data
     *
     * @var array
     */
    protected $vars;

    public function setData(array $vars = []): void
    {
        $default = ['header' => Settings::instance()->letter_header];

        $this->vars = array_merge($default, $vars);
    }
}