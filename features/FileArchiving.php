<?php

namespace Empu\Printer\Features;

use System\Models\File;

/**
 * Store archive
 */
trait FileArchiving
{
    public function archive(string $content): File
    {
        $isObsolete = true;
        if ($oldFile = $this->getArchive()) {
            $exsitingHash = sha1($oldFile->getContents());
            $isObsolete = sha1($content) !== $exsitingHash;
        }

        if ($isObsolete) {
            $file = new File;
            $file->fromData($content, $this->archiveName());
            $file->is_public = true;
        }
        else {
            $file = $oldFile;
        }

        if (method_exists($this, 'fileArchiving')) {
            $this->fileArchiving($file);
        }

        $file->save();

        if (method_exists($this, 'fileArchived')) {
            $this->fileArchived($file);
        }

        if ($isObsolete && $oldFile) {
            $oldFile->delete();
        }

        return $file;
    }

    public function getArchive(): ?File
    {
        $file = File::where('file_name', $this->archiveName())->first();

        return $file;
    }
}
