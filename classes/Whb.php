<?php

namespace Empu\Printer\Classes;

use Empu\Printer\Contracts\ArchivingContent;
use Empu\Printer\Contracts\EscTemplate;
use Empu\Printer\Contracts\PdfTemplate;
use Empu\Printer\Contracts\WebDirectPrint;
use Empu\Printer\Models\Settings;
use Illuminate\Support\Facades\Log;
use Mike42\Escpos\PrintConnectors\DummyPrintConnector;
use Mike42\Escpos\Printer;
use System\Models\File;

/**
 * Web hardware bridge
 */
class Whb
{
    /**
     * @var \Empu\Printer\Contracts\WebDirectPrint
     */
    protected $template;

    public static function make(WebDirectPrint $template): Whb
    {
        $templateName = $template->name();
        if (app()->bound($templateName)) {
            $template = resolve($templateName);
        }

        $instance = new self();
        $instance->init($template);

        return $instance;
    }

    public function init(WebDirectPrint $template)
    {
        $this->template = $template;
    }

    public function sendJob(array $vars = []): array
    {
        try {
            $content = $this->generateContent($this->template, $vars);
            $encodedContent = base64_encode($content);

            if ($this->template instanceof EscTemplate) {
                $printJobData = ['raw_content' => $encodedContent];
            }
            elseif ($this->template instanceof PdfTemplate) {
                $printJobData = [
                    'url' => 'printout.pdf',
                    'file_content' => $encodedContent,
                ];
            }

            $printJobData['qty'] = $this->printingCopies();
        } catch (\Throwable $th) {
            Log::error($th);

            return ['errorMessage' => $th->getMessage()];
        }

        return [
            'whbData' => array_merge([
                'type' => $this->template->printerMapKey(),
            ], $printJobData)
        ];
    }

    public function generateContent($template, array $vars = []): string
    {
        // FIX: setData tidak tercantum dalam interface
        $template->setData($vars);

        if ($template instanceof EscTemplate) {
            $content = $this->bufferString($template, $vars);
        }
        elseif ($template instanceof PdfTemplate) {
            $content = $this->pdfString($template, $vars);
        }

        $this->storeContent($template, $content);

        return $content;
    }

    protected function bufferString(EscTemplate $template, array $vars): string
    {
        $connector = new DummyPrintConnector();
        $printer = new Printer($connector);

        $printer->initialize();

        $template->draft($printer, $vars);

        $bufferString = $connector->getData();

        $printer->close();

        return $bufferString;
    }

    protected function pdfString(PdfTemplate $template, array $vars): string
    {
        $pdf = $template->draft($vars);

        return $pdf->output();
    }

    public function storeContent($template, string $content): ?File
    {
        return ($template instanceof ArchivingContent)
            ? $template->archive($content)
            : null;
    }

    protected function printingCopies(): int
    {
        $printingSetting = collect(Settings::get('print_settings', []))
            ->firstWhere('printer_map_key', $this->template->printerMapKey());

        return $printingSetting ? $printingSetting['copies'] : 1;
    }
}
