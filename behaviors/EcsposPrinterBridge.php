<?php

namespace Empu\Printer\Behaviors;

use Mike42\Escpos\PrintConnectors\DummyPrintConnector;
use Mike42\Escpos\Printer;
use Printer\BufferTransport;

class EcsposPrinterBridge
{
    /** @var \Mike42\Escpos\Printer */
    protected $printer;

    public function __construct()
    {
        $connector = new DummyPrintConnector();
        $this->printer = new Printer($connector);
        $this->printer->initialize();
    }

    /**
     * Get printer
     *
     * @return \Mike42\Escpos\Printer
     */
    public function getPrinter(): Printer
    {
        return $this->printer;
    }

    public function getBufferString(): string
    {
        $connector = $this->printer->getPrintConnector();
        $bufferTransport = new BufferTransport();
        $bufferString = $bufferTransport->stringify($connector);

        $this->printer->close();

        return $bufferString;
    }
}