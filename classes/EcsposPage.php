<?php

namespace Empu\Printer\Classes;

use Mike42\Escpos\Printer;

/**
 * @deprecated v1.0.2
 */
class EscposPage
{
    const FORMAT_ALIGN_LEFT = 0;
    const FORMAT_ALIGN_RIGHT = 1;
    const FORMAT_ALIGN_CENTER = 2;

    protected $printer;
    protected $lineLength;

    public function __construct(Printer $printer, int $lineLength = 80)
    {
        $this->printer = $printer;
        $this->lineLength = $lineLength;
    }

    public function line($text, $softLimit = true)
    {
        if (! $softLimit) {
            $text = substr($text, 0, $this->lineLength);
        }

        $this->printer->text("{$text}\n");
    }

    public function newLine($count = 1)
    {
        $text = str_repeat("\n", $count);

        $this->printer->text($text);
    }

    public function table(array $headers = [], array $rows = [], array $format = [])
    {
        foreach($headers as $i => $headerText) {
            $format[$i] = [
                'length' => $format[$i]['length'] ?? null,
                'align' => $format[$i]['align'] ?? self::FORMAT_ALIGN_LEFT,
            ];
        }
    }
}