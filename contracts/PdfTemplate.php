<?php

namespace Empu\Printer\Contracts;

use Barryvdh\Snappy\PdfWrapper;

interface PdfTemplate
{
    /**
     * Make pdf
     *
     * @param array $vars
     * @return \Barryvdh\Snappy\PdfWrapper
     */
    public function draft(array $vars = []): PdfWrapper;
}