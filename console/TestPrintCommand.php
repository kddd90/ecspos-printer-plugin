<?php namespace Empu\Printer\Console;

use Empu\Printer\Behaviors\EcsposPrinterBridge;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class TestPrintCommand extends Command
{
    /**
     * @var string The console command name.
     */
    protected $name = 'printer:test';

    /**
     * @var string The console command description.
     */
    protected $description = 'Test printer';

    /**
     * Execute the console command.
     * @return void
     */
    public function handle()
    {
        $bridge = new EcsposPrinterBridge();
        $printer = $bridge->getPrinter();
        $maxLines = $this->option('line');
        $maxChars = $this->option('char');

        for ($i = 0; $i < $maxLines; $i++) {
            $text = str_pad($i + 1, 10, '*') . str_repeat('0123456789', $i);
            $text = str_limit($text, $maxChars, '');
            $printer->text($text . "\n");
        }

        $bufferString = $bridge->getBufferString();
        $printerAddress = $this->argument('address');

        $this->sendBuffer($bufferString, $printerAddress);
        $this->output->writeln('Check your printer!');
    }

    protected function sendBuffer($bufferString, $address)
    {
        try {
            $fields = "buffer_string={$bufferString}";
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, "http://{$address}:8088/print");
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

            $response = curl_exec($ch);

            curl_close ($ch);

            $this->info("{$address} >> {$response}");
        } catch (\Throwable $th) {
            $this->error($th->getMessage());
        }
    }

    /**
     * Get the console command arguments.
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['address', InputArgument::REQUIRED, 'IP address of target printer server.']
        ];
    }

    /**
     * Get the console command options.
     * @return array
     */
    protected function getOptions()
    {
        return [
            ['line', 'l', InputOption::VALUE_OPTIONAL, 'How many lines will be print', 20],
            ['char', 'c', InputOption::VALUE_OPTIONAL, 'Characters per line', 80],
        ];
    }
}
